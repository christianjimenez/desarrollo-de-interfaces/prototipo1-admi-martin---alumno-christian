Trabajos a realizar

El plazo de entrega sera hasta el dia 10 a las 11:59 maximo
Toda la documentacion debera ser entregada por todos los integrantes del grupo

Documentaci�n

Todo el proceso de desarrollo de la aplicaci�n deber� ser documentado incluyendo al menos:
1. Recopilaci�n de las decisiones tomadas al respecto de los puntos se�alados en la secci�n
anterior
2. Cu�les son los factores diferenciales entre vuestra aplicaci�n y otros ejemplos que ya existen
en el mercado
3. Asignaci�n de trabajos por cada miembro del equipo
4. Manual de usuario de vuestra aplicaci�n
Presentaci�n (fecha entrega � presentaciones 4 nov.)
Exposici�n oral por parte de uno de los miembros del equipo en la que se contemplar�n al menos
los siguientes puntos teniendo una duraci�n de al menos 30 minutos sin superar los 60 minutos:

1. Cu�l fue la din�mica en la toma de decisiones respecto a la aplicaci�n
2. C�mo se organiz� el trabajo en equipo
3. Qu� papel tuvo cada miembro del equipo en el trabajo
4. Descripci�n de:
a. Requerimientos funcionales de vuestra aplicaci�n
b. Requerimientos no funcionales de la aplicaci�n
c. P�blico objetivo de vuestra aplicaci�n
d. Plataforma de vuestra aplicaci�n: M�vil/Web/Escritorio
e. Software con el que vais a desarrollar el prototipo
f. Dise�o de la aplicaci�n
g. Dise�o de las funciones de la aplicaci�n
h. Dise�o de las interacciones y comunicaciones con el usuario
i. Pol�tica de seguridad y/o privacidad de la aplicaci�n
5. Muestra del prototipo de la aplicaci�n
6. Defensa de los puntos diferenciales de vuestra aplicaci�n con respecto a otras alternativas de
mercado
